package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.MyController;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {


    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new MyController();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", " 1 - add new product to list");
        menu.put("2", " 2 - print product list");
        menu.put("3", " 3 - sort product list by price");
        menu.put("4", " 4 - sort product list by amount");
        menu.put("5", " 5 - create file product list");
        menu.put("Q", " Q - Exit");

        this.methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println("Please, add new product by template:");
        System.out.println("Name:(String)");
        String name = input.nextLine();
        System.out.println("price:(Integer)");
        int price = input.nextInt();
        System.out.println("Amount:(Integer)");
        int amount = input.nextInt();
        System.out.println("id:(Integer)");
        int id = input.nextInt();
        controller.addNewProduct(name, price, amount, id);
    }

    private void pressButton2() {
        System.out.println(controller.getProductList());
    }

    private void pressButton3() {
        System.out.println(controller.sortProductListByPrice());
    }

    private void pressButton4() {
        System.out.println(controller.sortProductListByAmount());
    }

    private void pressButton5() {
        controller.createProductFile();
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println("Wrong input, try another.");
            }
        } while (!keyMenu.equals("Q"));
    }
}
