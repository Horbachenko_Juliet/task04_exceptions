package com.epam.model.comparator;

import com.epam.model.Product;

import java.io.Serializable;
import java.util.Comparator;

public class PriceComparator implements Comparator<Product>, Serializable {
    @Override
    public int compare(Product p1, Product p2) {
        return Integer.compare(p1.getPriceOfProduct(), p2.getPriceOfProduct());
    }
}
