package com.epam.model;

import com.epam.model.comparator.AmountComparator;
import com.epam.model.comparator.PriceComparator;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Save all parameters and methods for Application.
 */
public class Product implements AutoCloseable {


    private static List<Product> productList;
    private static int count = 0;
    public List<Product> sortedList;
    private int priceOfProduct;
    private int id;
    private int amount;
    private String name;
    private int objectCount;

    public Product() {
        if (productList == null) {
            productList = new LinkedList<>();
        }
    }

    public Product(String name, int priceOfProduct, int amount, int id) {
        setCount(getCount() + 1);
        setName(name);
        setPriceOfProduct(priceOfProduct);
        setAmount(amount);
        setId(id);
        setObjectCount(getCount());
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Product.count = count;
    }

    public int getObjectCount() {
        return objectCount;
    }

    public void setObjectCount(int objectCount) {
        this.objectCount = objectCount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPriceOfProduct() {
        return priceOfProduct;
    }

    public void setPriceOfProduct(int priceOfProduct) {
        this.priceOfProduct = priceOfProduct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addNewProduct(String name, int price, int amount, int id) {
        productList.add(new Product(name, price, amount, id));
    }

    public List<Product> getProductList() {
        return productList;
    }

    public List<Product> sortProductListByPrice() {
        sortedList = new LinkedList<>();
        sortedList = productList;
        PriceComparator myPriceComparator = new PriceComparator();
        sortedList.sort(myPriceComparator);
        return sortedList;
    }

    public List<Product> sortProductListByAmount() {
        sortedList = new LinkedList<>();
        sortedList = productList;
        AmountComparator amountComparator = new AmountComparator();
        sortedList.sort(amountComparator);
        Collections.reverse(sortedList);
        return sortedList;
    }

    public void createProductFile() {
        try (PrintWriter myPrintWriter = new PrintWriter("ProductList.txt", StandardCharsets.UTF_8)) {
            for (Product p : productList) {
                myPrintWriter.println(p);
                //throw new RuntimeException("Main Exception");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public String toString() {
        return String.format("%n" + "%d. %s:price:%d$, amount:%d pieces, id:%d", getObjectCount(), getName(), getPriceOfProduct(), getAmount(), getId());
    }

    @Override
    public void close() throws Exception {
        System.out.println("Close");
        //throw new Exception("Close Exception");
    }
}
