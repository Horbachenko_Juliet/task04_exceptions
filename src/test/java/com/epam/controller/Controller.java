package com.epam.controller;

import com.epam.model.Product;

import java.util.List;

public interface Controller {

    void addNewProduct(String name, int price, int amount, int id);

    List<Product> getProductList();

    List<Product> sortProductListByPrice();

    List<Product> sortProductListByAmount();

    void createProductFile();

}
